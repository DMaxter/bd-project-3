-- Insert users
INSERT INTO utilizador (email, password) VALUES ('joaquim.alberto@bdist.pt', 'bemALIMENTADO');
INSERT INTO utilizador_regular VALUES ('joaquim.alberto@bdist.pt');
INSERT INTO utilizador (email, password) VALUES ('jess.estrada@bdist.pt', 'DoentePeloBenfica');
INSERT INTO utilizador_qualificado VALUES ('jess.estrada@bdist.pt');
INSERT INTO utilizador (email, password) VALUES ('manuel.lentilhas@bdist.pt', '1234567');
INSERT INTO utilizador_qualificado VALUES ('manuel.lentilhas@bdist.pt');
INSERT INTO utilizador (email, password) VALUES ('riczao@bdist.pt', 'largarBalas');
INSERT INTO utilizador_regular VALUES ('riczao@bdist.pt');

-- Insert places
INSERT INTO local_publico (nome, latitude, longitude) VALUES ('Tasca do Ze', '21.90727', '-56.75750');
INSERT INTO local_publico (nome, latitude, longitude) VALUES ('Igreja Santa Fe', '-12.47767', '81.55022');
INSERT INTO local_publico (nome, latitude, longitude) VALUES ('Motel da Praia', '-18.55683', '125.48738');
INSERT INTO local_publico (nome, latitude, longitude) VALUES ('Rua da Escola', '45.1568', '-45.123123');
INSERT INTO local_publico (nome, latitude, longitude) VALUES ('Bar O Bebedolas', '55.123423', '123.123123');

-- Insert items
INSERT INTO item (descricao, localizacao, latitude, longitude) VALUES ('Palavra mal escrita', 'Na casa de banho', '21.90727', '-56.75750');
INSERT INTO item (descricao, localizacao, latitude, longitude) VALUES ('Palavra mal escrita', 'Porta da casa de banho', '21.90727', '-56.75750');
INSERT INTO item (descricao, localizacao, latitude, longitude) VALUES ('Palavra mal escrita', 'Porta principal', '-12.47767', '81.55022');
INSERT INTO item (descricao, localizacao, latitude, longitude) VALUES ('Palavra mal escrita', 'Estrada', '45.1568', '-45.123123');
INSERT INTO item (descricao, localizacao, latitude, longitude) VALUES ('Palavra mal escrita/traduzida', 'Na saida do bar', '55.123423', '123.123123');

-- Insert anomalies
INSERT INTO anomalia (zona, imagem, lingua, ts, descricao, tem_anomalia_redacao) VALUES ('((1,1), (2,4))', 'https://bit.ly/2q0Y55u', 'en', '2018-07-12 08:13:30', 'SCOHOL', 'true');
INSERT INTO anomalia (zona, imagem, lingua, ts, descricao, tem_anomalia_redacao) VALUES ('((0,5), (10, 2))', 'https://bit.ly/2OSfNQL', 'pt', '2018-11-20 22:07:05', 'Distroio', 'true');
INSERT INTO anomalia (zona, imagem, lingua, ts, descricao, tem_anomalia_redacao) VALUES ('((5, 2), (7, 5))', 'https://bit.ly/33vm5vj', 'en', '2019-02-07 06:37:40', 'Aiports', 'true');
INSERT INTO anomalia_traducao (id, zona2, lingua2) VALUES ('3', '((5, 6), (7,9))', 'pt');
INSERT INTO anomalia (zona, imagem, lingua, ts, descricao, tem_anomalia_redacao) VALUES ('((1,5),(6,8))', 'https://bit.ly/2sn3ZyA', 'en', '2019-01-25 12:16:01', 'Termite', 'false');
INSERT INTO anomalia_traducao (id, zona2, lingua2) VALUES ('4', '((1,0),(6,8))', 'pt');
INSERT INTO anomalia (zona, imagem, lingua, ts, descricao, tem_anomalia_redacao) VALUES ('((1,2),(5,3))', 'https://bit.ly/2L3lg6a', 'pt', '2016-06-08 05:26:57', 'Ha luga-se um aneqso', 'true');
INSERT INTO anomalia (zona, imagem, lingua, ts, descricao, tem_anomalia_redacao) VALUES ('((1,1), (2,4))', 'https://bit.ly/2q0Y55u', 'en', '2016-06-16 17:38:49', 'Scohol', 'true');
INSERT INTO anomalia (zona, imagem, lingua, ts, descricao, tem_anomalia_redacao) VALUES ('((1,1), (2,4))', 'https://bit.ly/2q0Y55u', 'en', '2017-09-01 19:43:09', 'SCOHOL', 'true');

-- Insert duplicates
INSERT INTO duplicado (item1, item2) VALUES ('1', '2');

-- Insert incidences
INSERT INTO incidencia (anomalia_id, item_id, email) VALUES ('1', '4', 'joaquim.alberto@bdist.pt');
INSERT INTO incidencia (anomalia_id, item_id, email) VALUES ('6', '4', 'manuel.lentilhas@bdist.pt');
INSERT INTO incidencia (anomalia_id, item_id, email) VALUES ('2', '1', 'jess.estrada@bdist.pt');
INSERT INTO incidencia (anomalia_id, item_id, email) VALUES ('3', '5', 'joaquim.alberto@bdist.pt');
INSERT INTO incidencia (anomalia_id, item_id, email) VALUES ('4', '4', 'riczao@bdist.pt');

-- Insert correction proposals
INSERT INTO proposta_de_correcao (email, nro, data_hora, texto) VALUES ('jess.estrada@bdist.pt', '1', '2019-05-29 06:15:58', 'School');
INSERT INTO proposta_de_correcao (email, nro, data_hora, texto) VALUES ('jess.estrada@bdist.pt', '2', '2019-08-23 11:00:24', 'Destruo');
INSERT INTO proposta_de_correcao (email, nro, data_hora, texto) VALUES ('manuel.lentilhas@bdist.pt', '1', '2019-11-20 18:45:00', 'Airports');
INSERT INTO proposta_de_correcao (email, nro, data_hora, texto) VALUES ('manuel.lentilhas@bdist.pt', '2', '2018-08-01 00:43:24', 'School');

-- Insert correction
INSERT INTO correcao (email, nro, anomalia_id) VALUES ('jess.estrada@bdist.pt', '1', '1');
INSERT INTO correcao (email, nro, anomalia_id) VALUES ('manuel.lentilhas@bdist.pt', '1', '3');
INSERT INTO correcao (email, nro, anomalia_id) VALUES ('manuel.lentilhas@bdist.pt', '2', '6');
