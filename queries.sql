-- Local publico com mais anomalias registadas
SELECT X.latitude, X.longitude FROM item X, incidencia Z WHERE Z.item_id = X.id GROUP BY X.latitude, X.longitude HAVING COUNT(Z.item_id) >= ALL(SELECT COUNT(T.id) FROM item T, incidencia I WHERE I.item_id = T.id GROUP BY T.latitude, T.longitude);

-- Utilizador regular com mais anomalias de traducao no 1o semestre de 2019 registadas
SELECT Z.email FROM utilizador_regular Z, incidencia X, anomalia Y, anomalia_traducao W WHERE X.email = Z.email AND X.anomalia_id = W.id AND W.id = Y.id AND Y.ts BETWEEN '2019-01-01 00:00:00' AND '2019-06-30 23:59:59' GROUP BY Z.email HAVING COUNT(X.anomalia_id) >= ALL(SELECT COUNT(I.anomalia_id) FROM incidencia I, utilizador_regular U, anomalia_traducao T, anomalia A WHERE I.anomalia_id = A.id AND I.email = U.email AND A.ts BETWEEN '2019-01-01 00:00:00' AND '2019-06-30 23:59:59' AND A.id = T.id GROUP BY U.email);

-- Todos os utilizadores que registaram incidências em todos os locais publicos a norte de Rio Maior em 2019
SELECT DISTINCT * FROM utilizador U WHERE NOT EXISTS((SELECT T.latitude, T.longitude FROM anomalia A, item T, incidencia I WHERE T.latitude > 39.336775 AND T.id = I.item_id AND I.anomalia_id = A.id AND A.ts BETWEEN '2019-01-01 00:00:00' AND '2019-12-31 23:59:59') EXCEPT (SELECT T.latitude, T.longitude FROM item T, incidencia I WHERE U.email = I.email AND I.item_id = T.id AND T.latitude > 39.336775));

-- Todos os utilizadores qualificados que nao apresentaram propostas de correcao para as suas incidencias para locais a sul de Rio Maior no ano corrente
SELECT DISTINCT * FROM utilizador_qualificado U WHERE NOT EXISTS ((SELECT P.email, P.nro FROM proposta_de_correcao P, incidencia I, correcao C WHERE P.email = C.email AND I.email = C.email AND P.nro = C.nro AND C.anomalia_id = I.anomalia_id) EXCEPT (SELECT P.email, P.nro FROM item T, incidencia I, proposta_de_correcao P, correcao C, anomalia A WHERE U.email = I.email AND I.email = P.email AND I.item_id = T.id AND T.latitude < 39.336775 AND A.id = C.anomalia_id AND EXTRACT(YEAR FROM CURRENT_TIMESTAMP) = EXTRACT(YEAR FROM A.ts)));
