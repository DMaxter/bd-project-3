-- Drop tables
DROP TABLE IF EXISTS utilizador CASCADE;
DROP TABLE IF EXISTS utilizador_regular CASCADE;
DROP TABLE IF EXISTS utilizador_qualificado CASCADE;
DROP TABLE IF EXISTS local_publico CASCADE;
DROP TABLE IF EXISTS item CASCADE;
DROP TABLE IF EXISTS anomalia CASCADE;
DROP TABLE IF EXISTS anomalia_traducao CASCADE;
DROP TABLE IF EXISTS duplicado CASCADE;
DROP TABLE IF EXISTS incidencia CASCADE;
DROP TABLE IF EXISTS proposta_de_correcao CASCADE;
DROP TABLE IF EXISTS correcao CASCADE;

CREATE TABLE local_publico(
	latitude DECIMAL(9, 6) NOT NULL,
	longitude DECIMAL(9, 6) NOT NULL,
	nome VARCHAR(255) NOT NULL,

	CHECK(latitude >= -90 AND latitude <= 90 AND longitude >= -180 AND longitude <= 180),

	PRIMARY KEY (latitude, longitude)
);

CREATE TABLE item(
	id SERIAL NOT NULL,
	descricao VARCHAR(255) NOT NULL,
	localizacao VARCHAR(255),
	latitude DECIMAL(9, 6) NOT NULL,
	longitude DECIMAL(9, 6) NOT NULL,

	PRIMARY KEY (id),
	FOREIGN KEY (latitude, longitude) REFERENCES local_publico(latitude, longitude) ON DELETE CASCADE
);

CREATE TABLE anomalia(
	id SERIAL NOT NULL,
	zona BOX NOT NULL,
	lingua VARCHAR(255) NOT NULL,
	imagem VARCHAR(255) NOT NULL,
	ts TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
	descricao VARCHAR(255) NOT NULL,
	tem_anomalia_redacao BOOLEAN NOT NULL,

	PRIMARY KEY (id)
);

CREATE TABLE anomalia_traducao(
	id INT NOT NULL,
	zona2 BOX NOT NULL,
	lingua2 VARCHAR(255) NOT NULL,

	PRIMARY KEY (id),
	FOREIGN KEY (id) REFERENCES anomalia(id) ON DELETE CASCADE
);

CREATE TABLE duplicado(
	item1 INT NOT NULL,
	item2 INT NOT NULL,

	CHECK(item1 < item2),

	PRIMARY KEY (item1, item2),
	FOREIGN KEY (item1) REFERENCES item(id) ON DELETE CASCADE,
	FOREIGN KEY (item2) REFERENCES item(id) ON DELETE CASCADE
);

CREATE TABLE utilizador(
	email VARCHAR(255) NOT NULL,
	password VARCHAR(255) NOT NULL,

	PRIMARY KEY (email)
);

CREATE TABLE utilizador_regular(
	email VARCHAR(255) NOT NULL,

	PRIMARY KEY (email),
	FOREIGN KEY (email) REFERENCES utilizador(email) ON DELETE CASCADE
);

CREATE TABLE utilizador_qualificado(
	email VARCHAR(255) NOT NULL,

	PRIMARY KEY (email),
	FOREIGN KEY (email) REFERENCES utilizador(email) ON DELETE CASCADE
);

CREATE TABLE incidencia(
	anomalia_id INT NOT NULL,
	item_id INT NOT NULL,
	email VARCHAR(255) NOT NULL,

	PRIMARY KEY (anomalia_id),
	FOREIGN KEY (anomalia_id) REFERENCES anomalia(id) ON DELETE CASCADE,
	FOREIGN KEY (item_id) REFERENCES item(id) ON DELETE CASCADE,
	FOREIGN KEY (email) REFERENCES utilizador(email) ON DELETE CASCADE
);

CREATE TABLE proposta_de_correcao(
	email VARCHAR(255) NOT NULL,
	nro INT NOT NULL,
	data_hora TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL, -- FIXME not sure about this
	texto VARCHAR(255) NOT NULL,

	PRIMARY KEY (email, nro),
	FOREIGN KEY (email) REFERENCES utilizador_qualificado(email) ON DELETE CASCADE
);

CREATE TABLE correcao(
	email VARCHAR(255) NOT NULL,
	nro INT NOT NULL,
	anomalia_id INT NOT NULL,

	PRIMARY KEY (email, nro, anomalia_id),
	FOREIGN KEY (email, nro) REFERENCES proposta_de_correcao(email, nro) ON DELETE CASCADE,
	FOREIGN KEY (anomalia_id) REFERENCES incidencia(anomalia_id) ON DELETE CASCADE
);
