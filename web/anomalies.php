<html>
	<head>
		<title>Anomalias | Translate Right</title>
	</head>
	<body>
		<a href="index.php">Retroceder</a>
		<form id="anomWriteRem" method="POST">
			<input type="hidden" name="anomWriteRem" value="yes"/>
		</form>
		<form id="anomTransRem" method="POST">
			<input type="hidden" name="anomTransRem" value="yes"/>
		</form>
		<form id="anomRem" method="POST">
			<input type="hidden" name="anomRem" value="yes"/>
		</form>
		<?php
			include "config.php";

			// Sanity checks
			if(isset($_POST["anomalyInsert"])){
				if(!(isset($_POST["image"]) && isset($_POST["z1p1x"]) && isset($_POST["z1p1y"]) &&
						isset($_POST["z1p2x"]) && isset($_POST["z1p2y"]) && isset($_POST["language1"]) &&
						isset($_POST["writing"]) && isset($_POST["translation"]) && isset($_POST["description"]))){
					echo "<p>Preencha todos os campos</p>";
				}elseif($_POST["z1p1x"] < 0 || $_POST["z1p1y"] < 0 || $_POST["z1p2x"] < 0 || $_POST["z1p2y"] < 0){
					echo "<p>As coordenadas dos pontos devem ser maiores ou iguais a 0</p>";
				}elseif(strlen($_POST["description"]) > 255 || strlen($_POST["image"]) > 255){
					echo "<p>Descricao e imagem nao devem exceder 255 caracteres cada</p>";
				}elseif($_POST["translation"] == "yes" && ($_POST["z2p1x"] < 0 || $_POST["z2p1y"] < 0 ||
							$_POST["z2p2x"] < 0 || $_POST["z2p2y"] < 0)){
					echo "<p>As coordenadas dos pontos devem ser maiores ou iguais a 0</p>";
				}elseif($_POST["translation"] == "yes" && !(isset($_POST["z2p1x"]) && isset($_POST["z2p1y"]) &&
							isset($_POST["z2p2x"]) && isset($_POST["z2p2y"]) && isset($_POST["language2"]))){
						echo "<p>Preencha os campos da anomalia de traducao</p>";
				}elseif($_POST["translation"] == "yes" || ($_POST["translation"] == "no" && $_POST["writing"] == "yes")){
					$queryAnom = "INSERT INTO anomalia (zona, imagem, lingua, descricao, tem_anomalia_redacao) VALUES (:zona, :imagem, :lingua, :desc, :red) RETURNING id;";
					$queryTrad = "INSERT INTO anomalia_traducao (id, zona2, lingua2) VALUES (:id, :zona, :lingua)";

					// Insert anomaly
					try{
						$db->beginTransaction();

						$insert = $db->prepare($queryAnom);
						$insert->execute(array(":zona" => "((".$_POST["z1p1x"].",".$_POST["z1p1y"]."),(
															".$_POST["z1p2x"].",".$_POST["z1p2y"]."))",
												":lingua" => $_POST["language1"],
												":imagem" => $_POST["image"],
												":desc" => $_POST["description"],
												":red" => ($_POST["writing"] == "yes" ? true : false)));

						$id = $insert->fetchAll();

						// Check if translation anomaly
						if($_POST["translation"] == "yes"){
							$insert = $db->prepare($queryTrad);
							$insert->execute(array(":id" => $id[0]["id"],
													":zona" => "((".$_POST["z2p1x"].",".$_POST["z2p1y"]."),(
																".$_POST["z2p2x"].",".$_POST["z2p2y"]."))",
													":lingua" => $_POST["language2"]));
						}

						$db->commit();
						echo "<p>Anomalia inserida com sucesso</p>";
					}catch(PDOException $e){
						if($db->inTransaction()){
							$db->rollBack();
						}

						echo "<p>ERRO: Nao foi possivel inserir a anomalia</p>";
					}
				}
			}elseif(isset($_POST["anomWriteRem"]) && $_POST["anomWriteRem"] == "yes" && isset($_POST["anomaly"])){	
				$query = "UPDATE anomalia SET tem_anomalia_redacao = FALSE WHERE id = :id;";

				try{
					$update = $db->prepare($query);
					$update->execute(array(":id" => $_POST["anomaly"]));
					
					echo "<p>Anomalia de redacao removida com sucesso</p>";
				}catch(PDOException $e){
					echo "<p>ERRO: Nao foi possivel remover a anomalia de redacao</p>";
				}
			}elseif(isset($_POST["anomTransRem"]) && $_POST["anomTransRem"] == "yes" && isset($_POST["anomaly"])){
				$query = "DELETE FROM anomalia_traducao WHERE id = :id;";

				try{
					$remove = $db->prepare($query);
					$remove->execute(array(":id" => $_POST["anomaly"]));
					
					echo "<p>Anomalia de translacao removida com sucesso</p>";
				}catch(PDOException $e){
					echo "<p>ERRO: Nao foi possivel remover a anomalia de traducao</p>";
				}
			}elseif(isset($_POST["anomRem"]) && $_POST["anomRem"] == "yes" && isset($_POST["anomaly"])){
				$query = "DELETE FROM anomalia WHERE id = :id;";

				try{
					$remove = $db->prepare($query);
					$remove->execute(array(":id" => $_POST["anomaly"]));
					
					echo "<p>Anomalia removida com sucesso</p>";
				}catch(PDOException $e){
					echo "<p>ERRO: Nao foi possivel remover a anomalia</p>";
				}
			}

			$query = "SELECT A.id, imagem, descricao, ts, zona, lingua, tem_anomalia_redacao, zona2, lingua2 FROM anomalia AS A LEFT JOIN anomalia_traducao AS B ON A.id = B.id ORDER BY A.id ASC;";
			$hasAnomalies = false;

			// List all anomalies
			try{
				$result = $db->query($query);

				if($result->rowCount() != 0){
					$hasAnomalies = true;
					echo "<table>
							<thead>
								<tr>
									<td>ID</td>
									<td>Imagem</td>
									<td>Descricao</td>
									<td>TimeStamp</td>
									<td>Zona 1</td>
									<td>Lingua 1</td>
									<td>Anomalia de Redacao</td>
									<td>Zona 2</td>
									<td>Lingua 2</td>
									<td>Remocao</td>
								</tr>
							</thead>
							<tbody>";
				}

				foreach($result as $row){
					echo "<tr>
							<td>".$row["id"]."</td>
							<td>".$row["imagem"]."</td>
							<td>".$row["descricao"]."</td>
							<td>".$row["ts"]."</td>
							<td>".$row["zona"]."</td>
							<td>".$row["lingua"]."</td>
							<td>".($row["tem_anomalia_redacao"] ? "true" : "false")."</td>
							<td>".($row["zona2"] ? $row["zona2"] : "NULL")."</td>
							<td>".($row["lingua2"] ? $row["lingua2"] : "NULL")."</td>
							<td>";

					// Button to remove individual parts of the anomaly
					if($row["tem_anomalia_redacao"] && $row["lingua2"]){
						echo "<button type=\"submit\" name=\"anomaly\" form=\"anomWriteRem\" value=\"".$row["id"]."\">Remover Redacao</button>";
						echo "<button type=\"submit\" name=\"anomaly\" form=\"anomTransRem\" value=\"".$row["id"]."\">Remover Traducao</button>";
					}

					echo "<button type=\"submit\" name=\"anomaly\" form=\"anomRem\" value=\"".$row["id"]."\">Total</button></td>
						</tr>";
				}

				if($hasAnomalies){
					echo "</tbody>
						</table>";
				}else{
					echo "<p>Nao existem anomalias registadas</p>";
				}
			}catch(PDOException $e){
				echo "<p>ERRO: Nao foi possivel obter as anomalias registadas</p>";
			}

			$db = NULL;
		?>
		<h2>Inserir Anomalia</h2>
		<form method="post">
			<p>Imagem:</p>
			<input type="text" name="image" maxlength="255" required/>
			<p>Descricao:</p>
			<input type="text" name="description" maxlength="255" required/>
			<p>Zona:</p>
			<label>Ponto 1</label>
			<label>X: <input type="number" name="z1p1x" step="1" min="0" required/></label>
			<label>Y: <input type="number" name="z1p1y" step="1" min="0" required/></label><br>
			<label>Ponto 2</label>
			<label>X: <input type="number" name="z1p2x" step="1" min="0" required/></label>
			<label>Y: <input type="number" name="z1p2y" step="1" min="0" required/></label><br>
			<p>Lingua:</p>
			<input type="text" name="language1" maxlength="255" required/>
			<p>Anomalia de Redacao:</p>
			<label><input type="radio" name="writing" value="yes" required/>Sim</label>
			<label><input type="radio" name="writing" value="no" required/>Nao</label>
			<p>Anomalia de Traducao:</p>
			<label><input type="radio" name="translation" value="yes" required/>Sim</label>
			<label><input type="radio" name="translation" value="no" required/>Nao</label>
			<p>Zona 2:</p>
			<label>Ponto 1</label>
			<label>X: <input type="number" name="z2p1x" step="1" min="0"/></label>
			<label>Y: <input type="number" name="z2p1y" step="1" min="0"/></label><br>
			<label>Ponto 2</label>
			<label>X: <input type="number" name="z2p2x" step="1" min="0"/></label>
			<label>Y: <input type="number" name="z2p2y" step="1" min="0"/></label><br>
			<p>Lingua 2:</p>
			<input type="text" name="language2" maxlength="255"/><br>
			<input type="submit" name="anomalyInsert" value="Inserir"/>
		</form>
	</body>
</html>
