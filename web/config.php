<?php
	try{
		$dbname = "";
		$dbuser = "";
		$dbpass = "";
		$dbhost = "";

		$db = new PDO("pgsql:host=$dbhost;dbname=$dbname", $dbuser, $dbpass);
		$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	}catch (PDOException $e){
		echo "<p>ERRO: Nao foi possivel estabelecer conexao com a base de dados</p>";
	}

?>
