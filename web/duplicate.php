<html>
	<head>
		<title>Duplicados | Translate Right</title>
	</head>
	<body>
		<a href="index.php">Retroceder</a>
		<?php
			include "config.php";

			// Insert duplicate
			if(isset($_POST["duplicateInsert"])){
				if($_POST["item1"] == $_POST["item2"]){
					echo "<p>Um item nao pode ser duplicado de si proprio</p>";
				}else{
					$query = "INSERT INTO duplicado (item1, item2) VALUES (:i1, :i2);";

					if($_POST["item1"] > $_POST["item2"]){
						$temp = $_POST["item1"];
						$_POST["item1"] = $_POST["item2"];
						$_POST["item2"] = $temp;
					}

					try{
						$insert = $db->prepare($query);
						$insert->execute(array(":i1" => $_POST["item1"],
												":i2" => $_POST["item2"]));

						echo "<p>Duplicado inserido com sucesso</p>";
					}catch(PDOException $e){
						echo "<p>ERRO: Nao foi possivel inserir o duplicado</p>";
					}
				}
			}

			$query = "SELECT * FROM duplicado ORDER BY item1 ASC;";
			$hasDuplicates = false;

			try{
				$result = $db->query($query);

				if($result->rowCount() != 0){
					$hasDuplicates = true;
					echo "<table>
							<thead>
								<tr>
									<td>Item 1</td>
									<td>Item 2</td>
								</tr>
							</thead>
							<tbody>";
				}

				foreach($result as $row){
					echo "<tr>
							<td>".$row["item1"]."</td>
							<td>".$row["item2"]."</td>
						</tr>";
				}

				if($hasDuplicates){
					echo "</tbody>
						</table>";
				}else{
					echo "<p>Nao existem duplicados registados</p>";
				}
			}catch(PDOException $e){
				echo "<p>ERRO: Nao foi possivel obter os duplicados registados</p>";
			}

			$db = NULL;
		?>
		<h2>Inserir Duplicado</h2>
		<form method="post">
			<p>Item 1:</p>
			<select name="item1" required>
				<?php
					include "config.php";

					$query = "SELECT id, localizacao FROM item ORDER BY id ASC;";
					$items = "";

					try{
						$result = $db->query($query);

						$items = $result->fetchAll();
					}catch(PDOException $e){
						echo "<p>ERROR: Nao foi possivel obter os itens registados</p>";
					}

					foreach($items as $row){
						echo "<option value=\"".$row["id"]."\">".$row["id"].", ".$row["localizacao"]."</option>";
					}

					$db = NULL;
				?>
			</select>
			<p>Item 2:</p>
			<select name="item2" required>
				<?php
					foreach($items as $row){
						echo "<option value=\"".$row["id"]."\">".$row["id"].", ".$row["localizacao"]."</option>";
					}
				?>
			</select>
			<input type="submit" name="duplicateInsert" value="Inserir"/>
		</form>
	</body>
</html>
