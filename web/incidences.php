<html>
	<head>
		<title>Incidencias | Translate Right</title>
	</head>
	<body>
		<a href="index.php">Retroceder</a>
		<?php
			include "config.php";

			$queryInsert = "INSERT INTO incidencia (anomalia_id, item_id, email) VALUES (:anom, :item, :email);";


			if(isset($_POST["incidenciaInsert"])){
				if(!isset($_POST["anomalia"]) || !isset($_POST["item"]) || !isset($_POST["utilizador"])){
					echo "<p>Preencha todos os requisitos</p>";
				}
				else {
					try{
						$insert = $db->prepare($queryInsert);
						$insert->execute(array(":anom" => $_POST["anomalia"],
									":item" => $_POST["item"],
									":email" => $_POST["utilizador"]));
						echo "<p>Incidencia inserida com sucesso</p>";
					}catch(PDOException $e){
						echo "<p>ERRO: Nao foi possivel inserir a incidencia</p>";
					}
				}
			}

			$query = "SELECT * FROM incidencia ORDER BY anomalia_id;";
			$hasIncidences = false;

			try{
				$result = $db->query($query);

				if($result->rowCount() != 0){
					$hasIncidences = true;
					echo "<table>
							<thead>
								<td>Anomalia</td>
								<td>Item</td>
								<td>Email</td>
							</thead>
							<tbody>";
				}

				foreach($result as $row){
					echo "<tr>
							<td>".$row["anomalia_id"]."</td>
							<td>".$row["item_id"]."</td>
							<td>".$row["email"]."</td>
						</tr>";
				}

				if($hasIncidences){
					echo "</tbody>
						</table>";
				}else{
					echo "<p>Nao ha incidencias registadas</p>";
				}
			}catch(PDOException $e){
				echo "<p>ERRO: Nao foi possivel obter as incidencias registads</p>";
			}

			$db = NULL;
		?>
		<form method="post">
			<p>Anomalia:</p>
			<select id="anom" name="anomalia" required>
				<?php
					include "config.php";

					$query = "SELECT id FROM anomalia ORDER BY id ASC;";

					try{
						$result = $db->query($query);

						foreach($result as $row){
							echo "<option value=\"".$row["id"]."\">".$row["id"]."</option>";
						}
					}catch(PDOException $e){
						echo "<p>ERRO: Nao foi possivel obter as anomalias registadas</p>";
					}

					$db = NULL;
				?>
			</select>
			<p>Item:</p>
			<select id="item" name="item" required>
				<?php
					include "config.php";

					$query = "SELECT id FROM item ORDER BY id ASC;";

					try{
						$result = $db->query($query);

						foreach($result as $row){
							echo "<option value=\"".$row["id"]."\">".$row["id"]."</option>";
						}
					}catch(PDOException $e){
						echo "<p>ERRO: Nao foi possivel obter os itens registados</p>";
					}

					$db = NULL;
				?>
			</select>
			<p>Utilizador:</p>
			<select id="util" name="utilizador" required>
				<?php
					include "config.php";

					$query = "SELECT email FROM utilizador ORDER BY email;";

					try{
						$result = $db->query($query);

						foreach($result as $row){
							echo "<option value=\"".$row["email"]."\">".$row["email"]."</option>";
						}
					}catch(PDOException $e){
						echo "<p>ERRO: Nao foi possivel obter os utilizadores registados</p>";
					}

					$db = NULL;
				?>
			</select>
			<input type="submit" name="incidenciaInsert" value="Inserir"/>
		</form>
	</body>
</html>
