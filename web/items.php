<html>
	<head>
		<title>Itens | Translate Right</title>
	</head>
	<body>
		<a href="index.php">Retroceder</a>
		<form id="itemRemove" method="post">
			<input type="hidden" name="itemRemove" value="yes"/>
		</form>
		<?php
			include "config.php";

			// Insert item
			if(isset($_POST["itemInsert"])){
				if(!isset($_POST["description"]) && isset($_POST["location"]) && isset($_POST["coordinates"])){
					echo "<p>Preencha todos os campos</p>";
				}else{
					// Sanity checks
					if(strlen($_POST["description"]) > 255){
						echo "<p>A descricao nao pode exceder 255 caracteres</p>";
					}elseif(strlen($_POST["location"]) > 255){
						echo "<p>A localizacao nao pode exceder 255 caracteres</p>";
					}

					$query = "INSERT INTO item (descricao, localizacao, latitude, longitude) VALUES (:desc, :loc, :lat, :long);";

					$parsed = explode(",", $_POST["coordinates"]);

					try{
						$insert = $db->prepare($query);
						$insert->execute(array(":desc" => $_POST["description"],
												":loc" => $_POST["location"],
												":lat" => $parsed[0],
												":long" => $parsed[1]));

						echo "<p>Item inserido com sucesso</p>";
					}catch(PDOException $e){
						echo "<p>ERRO: Nao foi possivel inserir o item</p>";
					}
				}

			// Remove item
			}elseif(isset($_POST["itemRemove"]) && $_POST["itemRemove"] == "yes"){
				$query = "DELETE FROM item WHERE id = :id";

				try{
					$remove = $db->prepare($query);
					$remove->execute(array(":id" => $_POST["item"]));

					echo "<p>Item removido com sucesso</p>";
				}catch(PDOException $e){
					echo "<p>ERRO: Nao foi possivel remover o item</p>";
				}
			}

			$query = "SELECT * FROM item ORDER BY id ASC;";
			$hasItems = false;

			try{
				$result = $db->query($query);

				if($result->rowCount() != 0){
					$hasItems = true;
					echo "<table>
							<thead>
								<tr>
									<td>Id</td>
									<td>Descricao</td>
									<td>Localizacao</td>
									<td>Coordenadas</td>
									<td>Remocao</td>
								</tr>
							</thead>
							<tbody>";
				}

				foreach($result as $row){
					echo "<tr>
							<td>".$row["id"]."</td>
							<td>".$row["descricao"]."</td>
							<td>".$row["localizacao"]."</td>
							<td>(".$row["latitude"].", ".$row["longitude"].")</td>
							<td><button name=\"item\" type=\"submit\" form=\"itemRemove\" value=\"".$row["id"]."\"/>Remover</button></td>
						</tr>";
				}

				if($hasItems){
					echo "</tbody>
						</table>";
				}else{
					echo "<p>Nao existem itens registados</p>";
				}
			}catch(PDOException $e){
				echo "<p>ERRO: Nao foi possivel obter os itens registados</p>";
			}

			$db = NULL;
		?>
		<h2>Inserir Item</h2>
		<form method="post">
			<p>Descricao:</p>
			<input type="text" name="description" maxlength="255" required/>
			<p>Localizacao:</p>
			<input type="text" name="location" maxlength="255" required/>
			<p>Coordenadas:</p>
			<select id="coords" name="coordinates" required>
				<?php
					include "config.php";

					$query = "SELECT * FROM local_publico ORDER BY nome ASC;";

					try{
						$result = $db->query($query);

						foreach($result as $row){
							echo "<option value=\"".$row["latitude"].",".$row["longitude"]."\">".$row["nome"]." (".$row["latitude"].", ".$row["longitude"].")</option>";
						}
					}catch(PDOException $e){
						echo "<p>ERRO: Nao foi possivel obter os locais registados</p>";
					}

					$db = NULL;
				?>
			</select>
			<input type="submit" name="itemInsert" value="Inserir"/>
		</form>
	</body>
</html>
