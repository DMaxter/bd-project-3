<html>
	<head>
		<title>Anomalias entre 2 Locais Publicos | Translate Right</title>
	</head>
	<body>
		<a href="index.php">Retroceder</a>
		<?php
			include "config.php";

			$query = "SELECT A.id, A.zona, A.imagem, A.lingua, A.ts, A.descricao, A.tem_anomalia_redacao, AT.zona2, AT.lingua2 FROM 
						anomalia A, item T, incidencia I, anomalia_traducao AT WHERE A.id = I.anomalia_id AND
						I.item_id = T.id AND AT.id = A.id AND T.latitude BETWEEN :latitude1 AND :latitude2 AND
						T.longitude BETWEEN :longitude1 AND :longitude2 ORDER BY A.id ASC;";

			if(isset($_POST["localInsert"])){
				if(!isset($_POST["localPublico1"]) || !isset($_POST["localPublico2"])){
					echo "<p>Escolha 2 locais publicos</p>";
				}else{
					$hasAnomalies = false;

					$parsed1 = explode(",", $_POST["localPublico1"]);
					$parsed2 = explode(",", $_POST["localPublico2"]);

					if($parsed1[0] > $parsed2[0]) {
						$temp = $parsed1[0];
						$parsed1[0] = $parsed2[0];
						$parsed2[0] = $temp;
					}

					if($parsed1[1] > $parsed2[1]) {
						$temp = $parsed1[1];
						$parsed1[1] = $parsed2[1];
						$parsed2[1] = $temp;
					}

					try{

						$result = $db->prepare($query);
						$result->execute(array(":latitude1" => $parsed1[0],
												":latitude2" => $parsed2[0],
												":longitude1" => $parsed1[1],
												":longitude2" => $parsed2[1]));

			
						if($result->rowCount() != 0){
							$hasAnomalies = true;
							echo "<table>
									<thead>
										<tr>
											<td>Id</td>
											<td>Zona</td>
											<td>Imagem</td>
											<td>Lingua</td>
											<td>Timestamp</td>
											<td>Descricao</td>
											<td>Tem Anomalia Redacao</td>
											<td>Zona 2</td>
											<td>Lingua 2</td>
										</tr>
									</thead>
									<tbody>";
						}
			
						foreach($result as $row){
							echo "<tr>
									<td>".$row["id"]."</td>
									<td>".$row["zona"]."</td>
									<td>".$row["imagem"]."</td>
									<td>".$row["lingua"]."</td>
									<td>".$row["ts"]."</td>
									<td>".$row["descricao"]."</td>
									<td>".($row["tem_anomalia_redacao"] ? "true" : "false")."</td>
									<td>".($row["zona2"] ? $row["zona2"] : "NULL")."</td>
									<td>".($row["lingua2"] ? $row["lingua2"] : "NULL")."</td>
								</tr>";
						}
			
						if($hasAnomalies){
							echo "</tbody>
								</table>";
						}else{
							echo "<p>Nao existem anomalias registadas entre estes dois locais</p>";
						}
					}catch(PDOException $e){
						echo("<p>ERRO: Nao foi possivel obter as anomalias registadas</p>");
					}
				}
			}

			$db = NULL;
		?>
		<form method="post">
			<p>Local Publico 1:</p>
			<select id="local1" name="localPublico1" required>
				<?php
					include "config.php";

					$query = "SELECT * FROM local_publico ORDER BY nome ASC;";
					$places = "";

					try{
						$result = $db->query($query);
						$places  = $result->fetchAll();
					}catch(PDOException $e){
						echo "<p>ERRO: Nao foi possivel obter os locais registados</p>";
					}

					foreach($places as $row){
						echo "<option value=\"".$row["latitude"].",".$row["longitude"]."\">".$row["nome"]." (".$row["latitude"].", ".$row["longitude"].")</option>";
					}

					$db = NULL;
				?>
			</select>

			<p>Local Publico 2:</p>
			<select id="local2" name="localPublico2" required>
				<?php
					foreach($places as $row){
						echo "<option value=\"".$row["latitude"].",".$row["longitude"]."\">".$row["nome"]." (".$row["latitude"].", ".$row["longitude"].")</option>";
					}
				?>
			</select>
			<input type="submit" name="localInsert" value="Inserir"/>
		</form>
	</body>
</html>
