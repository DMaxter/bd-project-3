<html>
	<head>
		<title>Anomalias por Zona | Translate Right</title>
	</head>
	<body>
		<a href="index.php">Retroceder</a>
		<?php

			include "config.php";

			$hasAnomalies = false;

			// Sanity checks
			if(isset($_POST["Insert"])){
				if(!(isset($_POST["latitude"]) && isset($_POST["longitude"]) && isset($_POST["dX"]) && isset($_POST["dY"]))){
					echo "<p>Preencha todos os campos</p>";
				}elseif(!(is_numeric($_POST["latitude"]) && -90 <= $_POST["latitude"] && $_POST["latitude"] <= 90)){
					echo "<p>A latitude tem de ser um numero entre -90 e 90</p>";
				}elseif(!(is_numeric($_POST["longitude"]) && -180 <= $_POST["longitude"] && $_POST["longitude"] <= 180)){
					echo "<p>A longitude tem de ser um numero entre -180 e 180</p>";
				}elseif(!(is_numeric($_POST["dX"]) && is_numeric($_POST["dY"]) &&
						-90 <= (float) $_POST["latitude"] - abs((float) $_POST["dX"]) && (float) $_POST["latitude"] + abs((float) $_POST["dX"]) <= 90 &&
						-180 <= (float) $_POST["longitude"] - abs((float) $_POST["dY"]) && (float) $_POST["longitude"] + abs((float) $_POST["dY"]) <= 180)){
					echo "<p>dX e dY tem de ser numeros e a sua soma com a latitude e a longitude, respetivamente, tem de estar entre os limites de latitude e longitude</p>";
				}else{
					$lat1 = (float) $_POST["latitude"] - abs((float) $_POST["dX"]);
					$lat2 = (float) $_POST["latitude"] + abs((float) $_POST["dX"]);
					$lon1 = (float) $_POST["longitude"] - abs((float) $_POST["dY"]);
					$lon2 = (float) $_POST["longitude"] + abs((float) $_POST["dY"]);

					$query = "SELECT A.id, A.zona, A.imagem, A.lingua, A.ts, A.descricao, A.tem_anomalia_redacao, AT.zona2, AT.lingua2 FROM 
							anomalia A, item T, incidencia I, anomalia_traducao AT WHERE T.id = I.item_id AND I.anomalia_id = A.id AND A.id = AT.id AND
							A.ts >= (CURRENT_TIMESTAMP - INTERVAL '3 months') AND T.latitude BETWEEN :lat1 AND :lat2 AND 
							T.longitude BETWEEN :lon1 AND :lon2 ORDER BY A.id ASC;";

					try{
						$result = $db->prepare($query);
						$result->execute(array(":lat1" => $lat1,
												":lat2" => $lat2,
												":lon1" => $lon1,
												":lon2" => $lon2));

						if($result->rowCount() != 0){
							$hasAnomalies = true;

							echo "<table>
									<thead>
										<tr>
											<td>Id</td>
											<td>Zona</td>
											<td>Imagem</td>
											<td>Lingua</td>
											<td>Timestamp</td>
											<td>Descricao</td>
											<td>Tem Anomalia Redacao</td>
										</tr>
									</thead>
									<tbody>";
						}

						foreach($result as $row){
							echo "<tr>
									<td>".$row["id"]."</td>
									<td>".$row["zona"]."</td>
									<td>".$row["imagem"]."</td>
									<td>".$row["lingua"]."</td>
									<td>".$row["ts"]."</td>
									<td>".$row["descricao"]."</td>
									<td>".($row["tem_anomalia_redacao"] ? "true" : "false")."</td>
									<td>".($row["zona2"] ? $row["zona2"] : "NULL")."</td>
									<td>".($row["lingua2"] ? $row["lingua2"] : "NULL")."</td>
								</tr>";
						}

						if($hasAnomalies){
							echo "</tbody>
								</table>";
						}else{
							echo "<p>Nao existem anomalias registadas nesta zona</p>";
						}
					}catch(PDOException $e){
						echo("<p>ERRO: Nao foi possivel obter as anomalias registadas</p>");
					}
				}
			}

			$db = NULL;
		?>
		<form method="POST">
			<p>Latitude:</p>
			<input type="number" step="0.000001" min="-90" max="90" name="latitude" value="0" required/><br>
			<p>Longitude:</p>
			<input type="number" step="0.000001" min="-180" max="180" name="longitude" value="0" required/><br>
			<p>dX:</p>
			<input type="number" step="0.000001" min="-90" max="90" name="dX" value="0" required/><br>
			<p>dY:</p>
			<input type="number" step="0.000001" min="-180" max="180" name="dY" value="0" required/><br>
			<input type="submit" name="Insert" value="Inserir"/>
		</form>
	</body>
</html>
