<html>
	<head>
		<title>Utilizadores | Translate Right</title>
	</head>
	<body>
		<a href="index.php">Retroceder</a>
		<?php
			include "config.php";

			$hasUsers = false;

			$query = "SELECT *, 'Regular' AS tipo  FROM utilizador NATURAL JOIN utilizador_regular UNION
						(SELECT *, 'Qualificado' AS tipo FROM utilizador NATURAL JOIN utilizador_qualificado) ORDER BY email ASC;";

			try{
				$result = $db->query($query);

				if($result->rowCount() != 0){
					$hasUsers = true;

					echo "<table>
							<thead>
								<tr>
									<td>Tipo</td>
									<td>Email</td>
									<td>Password</td>
								</tr>
							</thead>
							<tbody>";
				}

				foreach($result as $row){
					echo "<tr>
							<td>".$row["tipo"]."</td>
							<td>".$row["email"]."</td>
							<td>".$row["password"]."</td>
						</tr>";
				}

				if($hasUsers){
					echo "</tbody>
						</table>";
				}else{
					echo "<p>Nao existem utilizadores registados</p>";
				}
			}catch(PDOException $e){
				echo("<p>ERRO: Nao foi possivel obter os utilizadores registados</p>");
			}

			$db = NULL;
		?>
	</body>
</html>
