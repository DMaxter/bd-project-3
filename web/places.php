<html>
	<head>
		<title>Locais | Translate Right</title>
	</head>
	<body>
		<a href="index.php">Retroceder</a>
		<form id="placeRemove" method="post">
			<input type="hidden" name="placeRemove" value="yes"/>
		</form>
		<?php
			include "config.php";

			// Insert new place
			if(isset($_POST["placeInsert"])){
				if(!(isset($_POST["place"]) && isset($_POST["latitude"]) && isset($_POST["longitude"]))){
					echo "<p>Preencha todos os campos</p>";
				}elseif(strlen($_POST["place"] > 255)){
					echo "<p>O nome do local nao pode exceder 255 caracteres</p>";
				}elseif(!(is_numeric($_POST["latitude"]) && -90 <= (float) $_POST["latitude"] && (float) $_POST["latitude"] <= 90)){
					echo "<p>A latitude tem de ser um numero entre -90 e 90</p>";
				}elseif(!(is_numeric($_POST["longitude"]) && -180 <= (float) $_POST["longitude"] && (float) $_POST["longitude"] <= 180)){
					echo "<p>A longitude tem de ser um numero entre -180 e 180</p>";
				}else{
					$query = "INSERT INTO local_publico (nome, latitude, longitude) VALUES (:nome, :lat, :long);";

					try{
						$insert = $db->prepare($query);
						$insert->execute(array(":nome" => $_POST["place"],
												":lat" => $_POST["latitude"],
												":long" => $_POST["longitude"]));

						echo "<p>Local inserido com sucesso</p>";
					}catch(PDOException $e){
						echo "<p>ERRO: Nao foi possivel inserir o local</p>";
					}
				}
			}elseif(isset($_POST["placeRemove"]) && $_POST["placeRemove"] == "yes"){
				$query = "DELETE FROM local_publico WHERE latitude = :lat AND longitude = :long;";
				$parsed = explode(",", $_POST["place"]);

				try{
					// Remove place
					$remove = $db->prepare($query);
					$remove->execute(array(":lat" => $parsed[0],
											":long" => $parsed[1]));

					echo "<p>Local removido com sucesso</p>";
				}catch(PDOException $e){
					echo "<p>ERRO: Nao foi possivel remover o local</p>";
				}
			}

			$hasPlaces = false;

			$query = "SELECT * FROM local_publico ORDER BY nome ASC;";

			try{
				$result = $db->query($query);

				// Check for results
				if($result->rowCount() != 0){
					echo "<table>
							<thead>
								<tr>
									<td>Local</td>
									<td>Coordenadas</td>
									<td>Remocao</td>
								</tr>
							</thead>
							<tbody>";

					$hasPlaces = true;
				}

				foreach($result as $row){
					echo "<tr>
							<td>".$row["nome"]."</td>
							<td>(".$row["latitude"].", ".$row["longitude"].")</td>
							<td><button name=\"place\" type=\"submit\" form=\"placeRemove\" value=\"".$row["latitude"].",".$row["longitude"]."\"/>Remover</button></td>
						</tr>";
				}

				if($hasPlaces){
					echo "</tbody>
						</table>";
				}else{
					echo "<p>Nao ha locais registados</p>";
				}
			}catch(PDOException $e){
				echo "<p>ERRO: Nao foi possivel obter os locais registados</p>";
			}

			$db = NULL;
		?>

		<form method="POST">
			<h2>Inserir Local</h2>
			<p>Nome do local:</p>
			<input type="text" name="place" maxlength="255" required/><br>
			<p>Latitude:</p>
			<input type="number" step="0.000001" min="-90" max="90" name="latitude" value="0" required/><br>
			<p>Longitude:</p>
			<input type="number" step="0.000001" min="-180" max="180" name="longitude" value="0" required/><br>
			<input type="submit" name="placeInsert" value="Inserir"/>
		</form>
	</body>
</html>
