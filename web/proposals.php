<html>
	<head>
		<title>Propostas de Correcao | Translate Right</title>
	</head>
	<body>
		<a href="index.php">Retroceder</a>
		<form id="proposalRemove" method="post">
			<input type="hidden" name="proposalRemove" value="yes"/>
		</form>
		<?php
			include "config.php";

			// Insert proposal
			if(isset($_POST["proposalInsert"])){
				if(!isset($_POST["correcao"]) && isset($_POST["email"])){
					echo "<p>Preencha todos os campos</p>";
				}
				else{
					$email = $_POST["email"];

					// Sanity checks
					if(strlen($_POST["correcao"]) > 255){
						echo "<p>A correcao nao pode exceder 255 caracteres</p>";
					}elseif(strlen($_POST["email"]) > 255){
						echo "<p>O email nao pode exceder 255 caracteres</p>";
					}

					$query1 = "INSERT INTO proposta_de_correcao (email, nro, texto) VALUES (:email, :nro, :ctext);";
					$query3 = "SELECT MAX(nro) FROM proposta_de_correcao WHERE email = :email;";
					 
					try{
						$db->beginTransaction();

						$nRows = $db->prepare($query3);
						$nRows->execute(array(":email" => $email));
						$nRows = $nRows->fetchColumn();

						$query2 = "INSERT INTO correcao (email, nro, anomalia_id) VALUES (:email, :nro, :id);";
						$insert = $db->prepare($query1);
						$insert->execute(array(":ctext" => $_POST["correcao"],
												":email" => $_POST["email"],
												":nro" => $nRows+1));

						$insert = $db->prepare($query2);
						$insert->execute(array(":email" => $_POST["email"],
												":nro" => $nRows+1,
												":id" => $_POST["anomaly"]));

						$db->commit();

						echo "<p>Proposta de correcao inserida com sucesso</p>";
					}catch(PDOException $e){
						if($db->inTransaction()){
							$db->rollback();
						}

						echo "<p>ERRO: Nao foi possivel inserir a proposta de correcao</p>";
					}
				}
			
			}// Remove proposal
			elseif(isset($_POST["proposalRemove"]) && $_POST["proposalRemove"] == "yes"){
				$query = "DELETE FROM proposta_de_correcao WHERE email = :email and nro = :nro;";
				$parsed = explode(",", $_POST["proposal"]);

				try{
					$remove = $db->prepare($query);
					$remove->execute(array(":email" => $parsed[0],
											":nro" => $parsed[1]));

					echo "<p>Proposta de correcao removida com sucesso</p>";
				}catch(PDOException $e){
					echo "<p>ERRO: Nao foi possivel remover a proposta de correcao</p>";
				}
			}// EDIT proposal
			elseif(isset($_POST["proposalEdit"])){
				if(!isset($_POST["etext"])){
					echo "<p>Preencha o campo com o texto para editar</p>";
				}else{
					$etext = $_POST["etext"];
					$parsed = explode(",", $_POST["proposal"]);

					$query = "UPDATE proposta_de_correcao SET texto = :etext WHERE email = :email and nro = :nro;";

					try{
						$remove = $db->prepare($query);
						$remove->execute(array(":etext" => $etext,
												":email" => $parsed[0],
												":nro" => $parsed[1]));

						echo "<p>Proposta de correcao editada com sucesso</p>";
					}catch(PDOException $e){
						echo "<p>ERRO: Nao foi possivel editar a proposta de correcao</p>";
					}
				}
			}

			$query = "SELECT * FROM proposta_de_correcao NATURAL JOIN correcao ORDER BY email, nro ASC;";
			$hasProposals = false;

			try{
				$result = $db->query($query);

				if($result->rowCount() != 0){
					$hasProposals = true;
					echo "<table>
							<thead>
								<tr>
									<td>Email</td>
									<td>Nro</td>
									<td>Data e Hora</td>
									<td>Texto</td>
									<td>Anomalia</td>
									<td>Remocao</td>
									<td>Editar</td>
								</tr>
							</thead>
							<tbody>";
				}

				foreach($result as $row){
					echo "<tr>
							<td>".$row["email"]."</td>
							<td>".$row["nro"]."</td>
							<td>".$row["data_hora"]."</td>
							<td>".$row["texto"]."</td>
							<td>".$row["anomalia_id"]."</td>
							<td><button name=\"proposal\" type=\"submit\" form=\"proposalRemove\" value=\"".$row["email"].",".$row["nro"]."\"/>Remover</button></td>
							<td><form method=\"POST\"><input type=\"text\" name=\"etext\" maxlength=\"255\" required/>
							<input type=\"hidden\" name=\"proposal\" value=\"".$row["email"].",".$row["nro"]."\"/>
							<input type=\"submit\" name=\"proposalEdit\" value=\"Editar\"/></form></td>
						</tr>";
				}

				if($hasProposals){
					echo "</tbody>
						</table>";
				}else{
					echo "<p>Nao existem propostas de correcao registadas</p>";
				}
			}catch(PDOException $e){
				echo "<p>ERRO: Nao foi possivel obter as propostas de correcao</p>";
			} 

			$db = NULL;
		?>
		<h2>Inserir Proposta de Correcao</h2>
		<form method="post">
			<p>Correcao:</p>
			<input type="text" name="correcao" maxlength="255" required/>
			<p>Anomalia:</p>
			<select id="anomaly" name="anomaly" required>
				<?php
					include "config.php";

					$query = "SELECT anomalia_id FROM incidencia ORDER BY anomalia_id ASC;";

					try{
						$result = $db->query($query);

						foreach($result as $row){
							echo "<option value=\"".$row["anomalia_id"]."\">".$row["anomalia_id"]."</option>";
						}
					}catch(PDOException $e){
						echo "<p>ERRO: Nao foi possivel obter as anomalias registadas</p>";
					}

					$db = NULL;
				?>
			</select>
			<p>Email:</p>
			<select id="email" name="email" required>
				<?php
					include "config.php";

					$query = "SELECT * FROM utilizador_qualificado ORDER BY email ASC;";

					try{
						$result = $db->query($query);

						foreach($result as $row){
							echo "<option value=\"".$row["email"]."\">".$row["email"]."</option>";
						}
					}catch(PDOException $e){
						echo "<p>ERRO: Nao foi possivel obter os utilizadores qualificados registados</p>";
					}

					$db = NULL;
				?>
			</select>
			<input type="submit" name="proposalInsert" value="Inserir"/>
		</form>
	</body>
</html>
